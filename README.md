<a href="https://www.steema.com/product/html5">
<img align="right" src="https://www.steema.com/img/logos/teechart_html5.png">
</a>

100% JavaScript charting library using the HTML5 Canvas.

## History

[TeeChart JavaScript/HTML5](https://www.steema.com/product/html5), is a charting library maintained by the team at [Steema Software](https://www.steema.com).  

Steema Software publishes the TeeChart Javascript code for unrestricted use in non-commercial applications but continues to maintain and support the software for paid, commercial use.

See [paid support subscriptions](https://www.steema.com/product/html5#pricing).

## Live demos

## [HTML5](https://www.steema.com/files/public/teechart/html5/latest/demos/)

Find the demo sources [here](https://gitlab.com/steema/teechartjs/-/tree/main/ES5/demos).

## Resources
* ### [Tutorials](https://github.com/Steema/TeeChartJS/wiki)
* ### [API Reference](https://www.steema.com/docs/TeeChartHTML5Reference.htm)
* ### [Issue Tracker](http://bugs.steema.com/buglist.cgi?product=HTML5%20JavaScript%20TeeChart&query_format=advanced&resolution=---)

<p align="center">
<a href="https://www.steema.com/">
<img src="https://raw.githubusercontent.com/wiki/Steema/TeeChartJS/logo-steema.png">
</a>
</p>

### Use VSCode with npm on Windows or Linux to run the Features Demo.
 
1. install VSCode from [here](https://code.visualstudio.com/).
2. install the npm packages by running `npm i` 
3. run the code with `RUN AND DEBUG: Launch via npm`
4. open the app on [http://localhost:10000](http://localhost:10000)